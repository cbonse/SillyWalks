import unittest

from SillyWalks.sillywalks.noiseGeneration import add_simple_noise
from SillyWalks.sillywalks.noiseGeneration import add_complex_noise

class NoiseTestCase(unittest.TestCase):

    def test_add_simple_noise_empty_traces(self):
        x, y = [], []
        self.assertRaises(AssertionError, add_simple_noise, x, y, 0)

    def test_add_simple_noise_unqeual_traces(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,0]
        self.assertRaises(AssertionError, add_simple_noise, \
                          trace_x, trace_y, 0)

    def test_add_simple_noise_zero_noise(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,1,2,2]
        xn, yn = add_simple_noise(trace_x, trace_y, 0)
        self.assertEqual(trace_x, xn)
        self.assertEqual(trace_y, yn)

    def test_add_complex_noise_empty_traces(self):
        x, y = [], []
        cov = [[1,0],[0,1]]
        self.assertRaises(AssertionError, add_complex_noise, x, y, cov)

    def test_add_complex_noise_unqeual_traces(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,0]
        cov = [[1,0],[0,1]]
        self.assertRaises(AssertionError, add_complex_noise, trace_x, \
                          trace_y, cov)

    def test_add_complex_noise_zero_noise(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,1,2,2]
        cov = [[0,0],[0,0]]
        xn, yn = add_complex_noise(trace_x, trace_y, cov)
        self.assertEqual(trace_x, xn)
        self.assertEqual(trace_y, yn)

    def test_add_complex_noise_invalid_cov(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,1,2,2]
        cov = [[-1,3],[3,-10]] # neg. definit
        self.assertRaises(AssertionError, add_complex_noise, trace_x, \
                          trace_y, cov)
