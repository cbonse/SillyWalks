import os
import unittest

from SillyWalks.sillywalks.mapCreation import image2array
from SillyWalks.sillywalks.validation import validate_point
from SillyWalks.sillywalks.validation import validate_trace


class ValidationTestCase(unittest.TestCase):
    def setUp(self):
        self.home = os.path.dirname(os.path.abspath(__file__))
        self.config = {}
        image_path = os.sep.join((self.home,"maps","rightWall_6x4.png"))
        self.worldMap = image2array(image_path)

    def test_validate_point_negative_values(self):
        self.assertRaises(ValueError, validate_point, -1, -1, self.worldMap)

    def test_validate_point_too_big_values(self):
        self.assertRaises(ValueError, validate_point, 6, 4, self.worldMap)

    def test_validate_point_correct_validation_free_space(self):
        self.assertTrue(validate_point(0, 0, self.worldMap))

    def test_validate_point_correct_validation_occupied_pace(self):
        self.assertFalse(validate_point(5, 1, self.worldMap))

    def test_validate_trace_empty_traces(self):
        x, y = [], []
        self.assertRaises(AssertionError, validate_trace, x, y, self.worldMap)

    def test_validate_tace_unqeual_traces(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,0]
        self.assertRaises(AssertionError, validate_trace, trace_x, trace_y, self.worldMap)

    def test_validate_tace_invalid_trace(self):
        trace_x = [0,4,5,4,0]
        trace_y = [0,0,1,2,2]
        self.assertFalse(validate_trace(trace_x, trace_y, self.worldMap))

    def test_validate_tace_valid_trace(self):
        trace_x = [0,4,4,4,0]
        trace_y = [0,0,1,2,2]
        self.assertTrue(validate_trace(trace_x, trace_y, self.worldMap))
