r"""Tests for the Occupancy-grid-creation functions.

"""
import os
import numpy as np
import unittest

from SillyWalks.sillywalks.mapCreation import image2array

class ImageConverter(unittest.TestCase):

    def setUp(self):
        home = os.path.dirname(os.path.abspath(__file__))
        self.mapDir = os.sep.join((home,"maps"))+os.sep

    def check_array_equality(self, result, desiredResult):
        try:
            np.testing.assert_array_almost_equal(result, desiredResult)
            return True
        except AssertionError:
            print("Desired Result:")
            print(desiredResult)
            print("Actually got:")
            print(result)
            return False

    def test_mapCreation_invalid_path(self):
        path = "Yo Mama s fat!"
        self.assertRaises(IOError, image2array, path)

    def test_mapCreation_empty(self):
        path = self.mapDir+"empty_6x4.png"
        result = image2array(path)
        desiredResult = np.zeros((4,6))
        equal = self.check_array_equality(result, desiredResult)
        self.assertTrue(equal)

    def test_mapCreation_rightWall(self):
        path = self.mapDir+"rightWall_6x4.png"
        result = image2array(path)
        desiredResult = np.zeros((4,6))
        desiredResult[:,5] = 1
        equal = self.check_array_equality(result, desiredResult)
        self.assertTrue(equal)

    def test_mapCreation_rightWall_with_Color(self):
        path = self.mapDir+"rightWallWithColor_6x4.png"
        result = image2array(path)
        desiredResult = np.zeros((4,6))
        desiredResult[:,5] = 1
        resEquality = self.check_array_equality(result, desiredResult)
        self.assertTrue(resEquality)


if __name__ == "__main__":
    unittest.main()
